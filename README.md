*Open DICOMweb Toolkit*
# Utilities #


### Utility libraries for Open DICOMweb toolkit and applications ###

This package contains constants, functions, and classes that are not specifically related to DICOMweb, but are used by the Toolkit. The utilities are either:
    -1. generally useful, or
    -2. generally useful for DICOMweb or MINT applications.
    
* Version 0.1.3
* See Open DICOMweb Toolkit  //TODO add link


The following sub-libraries are included in **Utilities**

  **ascii**   
A set of constants and functions that are generally useful for dealing with ASCII (Uint8List) in Dart.

  **benchmark**   
A simple benchmarking toolkit - used for quick and dirty timings.

  **indenter**    
      A utility class for creating nested indentation. 
      
  **odw_logging**   
      A set of easy to use objects and methods for logging Open DICOMweb messages to a console, file, or both.
      
  **uuid**    
      A package for generating and using UUIDs.
      
  **version**   
      A simple package to aid in semantic versioning.
      
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jim Philbin <james.philbin>@jhmi.edu
* Other community or team contact