// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library Enum;

/**
 * Emulation of Java Enum class.
 *
 * Example:
 *
 * class Meter<int> extends Enum<int> {
 *
 *  const Meter(int val) : super (val);
 *
 *  static const Meter HIGH = const Meter(100);
 *  static const Meter MIDDLE = const Meter(50);
 *  static const Meter LOW = const Meter(10);
 * }
 *
 * and usage:
 *
 * assert (Meter.HIGH, 100);
 * assert (Meter.HIGH is Meter);
 */
abstract class Enum<T> {

  final T _value;

  const Enum(this._value);

  T get value => _value;
}
/*
// Example Enum
class Meter<int> extends Enum<int> {

  const Meter(int val) : super (val);

  static const Meter HIGH = const Meter(100);
  static const Meter MIDDLE = const Meter(50);
  static const Meter LOW = const Meter(10);
}

// usage:
//  assert (Meter.HIGH, 100);
//  assert (Meter.HIGH is Meter);

class Fruits<String> extends Enum {

  const Fruits(String val) : super(val);

  toString() => 'Enum.Fruits.${super.value}';

  static const APPLE = const Fruits('APPLE');
  static const PEAR = const Fruits('PEAR');
  static const BANANA = const Fruits('BANANA');
}

void main() {
  var yummy = Fruits.BANANA;

  print(Fruits.BANANA);

  switch (yummy) {
    case Fruits.APPLE:
      print('an apple a day');
      break;
    case Fruits.PEAR:
      print('genus Pyrus in the family Rosaceae');
      break;
    case Fruits.BANANA:
      print('open from the bottom, it is easier');
      break;
  }
}
*/
