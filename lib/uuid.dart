// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library uuid;

//TODO
// 1. Finish unfinished procedures
// 2. Implement version 2 and 3 UUIDs?.
// 3. Allow _uuidGenerator to specify the RNG
// 5. Create, Run and Pass Tests
// 6. Finish documentation

import 'dart:typed_data';
import 'package:uuid/uuid.dart' as BaseUuid;
import 'package:logging/logging.dart';

final Logger _log = new Logger("Uuid");

// Creates a new generator for [UUID]s
BaseUuid.Uuid _uuidGenerator = new BaseUuid.Uuid();

/**
 * Generates [UUID]s in v1 (default), v4, or v5 formats.
 */
class Uuid implements Comparable {
  final Uint8List  _uuid;
  // version = x.y, where x is 1 digit version and y is 1 digit variant
  final String     _version;

  // Private Constructor
  Uuid._(this._uuid, [String this._version]) {
    if (_uuid.length != 16) throw new ArgumentError('UUID length != 16');
    _log.finest('Uuid._:$_uuid');
  }

  // Factory Constructors
  factory Uuid.fromList(Uint8List bytes) {
    Uuid uuid = new Uuid._(bytes);
    _log.finer('Uuid.fromString Uuid:$bytes');
    return uuid;
  }

  factory Uuid.fromString(String s) {
    Uint8List bytes = new Uint8List.fromList( _uuidGenerator.parse(s));
    Uuid uuid = new Uuid._(bytes);
    _log.finer('Uuid.fromString Uuid:$bytes');
    return uuid;
  }

  // This factory is here primarily for testing
  factory Uuid._fromUint64s(int msb, int lsb) {
    ByteData bd = new ByteData(16);
    bd.setUint64(0, msb);
    bd.setUint64(8, lsb);
    var uuid = new Uuid._(new Uint8List.view(bd.buffer), null);
    _log.finer('Uuid._fromUint64s Uuid:$bd');
    return uuid;
  }

  factory Uuid.v1() => new Uuid._(_uuidGenerator.v1(), '1.2');

  //TODO finish
  factory Uuid.v2() { throw new UnimplementedError("V2 UUID's not yet implemented"); }

  //TODO finish
  factory Uuid.v3() { throw new UnimplementedError("V3 UUID's not yet implemented"); }

  factory Uuid.v4() => new Uuid._(_uuidGenerator.v4(buffer: new Uint8List(16)), '4.0');

  factory Uuid.random() => new Uuid._(_uuidGenerator.v4(buffer: new Uint8List(16)), '4.0');

  factory Uuid.v5(String namespace, String name) =>
      new Uuid._(_uuidGenerator.v5(namespace, name), '5.0');

  // Getters
  int    get mostSigBits  {
    ByteData  bd = new ByteData.view(_uuid.buffer);
    return bd.getUint64(0);
  }

  int    get leastSigBits {
    ByteData  bd = new ByteData.view(_uuid.buffer);
    return bd.getUint64(8);
  }

  // Operators
  String get version => _version;

  // Methods
  @override
  bool operator == (Uuid other) =>
    ((mostSigBits == other.mostSigBits)  &&
        (leastSigBits == other.leastSigBits) &&
        (version == other.version));

  @override
  int compareTo(Uuid other) {
    if (mostSigBits > other.mostSigBits) return 1;
    if (mostSigBits < other.mostSigBits) return -1;
    if (mostSigBits == other.mostSigBits) {
      if (leastSigBits > other.leastSigBits) return 1;
      if (leastSigBits < other.leastSigBits) return -1;
      if (leastSigBits == other.leastSigBits) return 0;
    }
    throw new ArgumentError('invalid comparison with $other');
  }

  /**
   * Returns a String representing this UUID.
   * The UUID string representation is described by this BNF:
   *
   * UUID                   = <time_low> "-" <time_mid> "-"
   *                          <time_high_and_version> "-"
   *                          <variant_and_sequence> "-"
   *                          <node>
   * time_low               = 4*<hexOctet>
   * time_mid               = 2*<hexOctet>
   * time_high_and_version  = 2*<hexOctet>
   * variant_and_sequence   = 2*<hexOctet>
   * node                   = 6*<hexOctet>
   * hexOctet               = <hexDigit><hexDigit>
   * hexDigit               = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
   *                              | "a" | "b" | "c" | "d" | "e" | "f"
   */
  @override
  toString() => _uuidGenerator.unparse(_uuid);

  String debug() => 'UUID$_version:$_uuid';
}