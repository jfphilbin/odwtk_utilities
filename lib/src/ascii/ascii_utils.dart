// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library ascii_utils;

import 'package:logging/logging.dart';

import './ascii_constants.dart';

final log = new Logger("utilities.string_utils");

// Useful utility functions

/// Returns True if the [char] is in the class [AsciiCharClass.DIGIT]
bool isDigit(int char)      => (char >= $_DIGIT_0) && (char <= $_DIGIT_9);

/// Returns [true] if [char] is an uppercase character.
bool isUppercase(int char)  => (char >= $_A) && (char <= $_Z);

/// Returns [false] if [char] is an lowercase character.
bool isLowercase(int char)  => (char >= $_a) && (char <= $_z);

/// Returns [true] if [char] is the ASCII space (' ') character.
bool isSpace(int char)      => char == $_SPACE;

/// Returns [true] if [char] is a whitespace character.
bool isWhitespace(int char) => (char == $_SPACE) || (char == $_HT);  //HT = Horizontal Tab

/// Returns [true] if [char] is a visable or printable character.
bool isVisable(int char)    => (char > $_SPACE) && (char < $_DEL);

/// Returns the integer value of a DIGIT or [null] otherwise.
int digitValue(int char)    => (isDigit(char)) ? char - $_DIGIT_0 : null;

bool inRange(int val, int min, int max) => (min <= val) && (val <= max);

/**
 * Returns a hex [String] with specified [padLeft] padding and leading "0x", if
 * [prefix] is true.
 */
String intToHex(int i, [int padLeft = 0, bool prefix = true]) {
  String s = i.toRadixString(16);
  s = s.padLeft(padLeft, "0");
  return (prefix) ? s = "0x" + s : s;
}

/// Returns a [List] of hex [Strings] mapped from [list]
Iterable<String> intListToHex(List<int>   list) => list.map(intToHex);

/// Returns a [String] in the form of a [List] ("[
String hexListToString(list) => "[" + list.join(", ") + "]";

/// /// A function that maps an [int> to a [String] in [fmt] format
typedef String IntToString(int i);

/// A function that maps a [List<int>] to a [String].
typedef String IntListToString(List<int> l);

/// A function that maps a [List<int>] to a [String] in [format].
IntListToString intListToString(IntToString format) {
   return (List<int> list) { return  "[" + list.map(format).join(", ") + "]"; };
   }

/** Returns a [String] in [List<HexString>] format. */
IntListToString intListToHexString = intListToString(intToHex);



