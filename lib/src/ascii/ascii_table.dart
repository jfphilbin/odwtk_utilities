// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library ascii_table;

import './ascii_char_class.dart';

class AsciiTable {
  final int code;
  final String name;
  final String description;
  final CharClass type;

  const AsciiTable(this.code, this.name, this.description, this.type);

  static const NUL = const AsciiTable(0,  'NUL', 'Null Character',            CharClass.CONTROL);
  static const SOH = const AsciiTable(1,  'SOH', 'Start of Header',           CharClass.CONTROL);
  static const STX = const AsciiTable(2,  'STX', 'Start of Text',             CharClass.CONTROL);
  static const ETX = const AsciiTable(3,  'ETX', 'End of Text',               CharClass.CONTROL);
  static const EOT = const AsciiTable(4,  'EOT', 'End of Transmission',       CharClass.CONTROL);
  static const ENQ = const AsciiTable(5,  'ENQ', 'Enquiry',                   CharClass.CONTROL);
  static const ACK = const AsciiTable(6,  'ACK', 'Acknowledgment',            CharClass.CONTROL);
  static const BEL = const AsciiTable(7,  'BEL', 'Bell',                      CharClass.CONTROL);
  static const BS  = const AsciiTable(8,  'BS',  'Backspace',                 CharClass.CONTROL);
  static const HT  = const AsciiTable(9,  'HT',  'Horizontal Tab',            CharClass.CONTROL);
  static const LF  = const AsciiTable(10, 'LF',  'Line Feed',                 CharClass.CONTROL);
  static const VT  = const AsciiTable(11, 'VT',  'Vertical Tab',              CharClass.CONTROL);
  static const FF  = const AsciiTable(12, 'FF',  'Form Feed',                 CharClass.CONTROL);
  static const CR  = const AsciiTable(13, 'CR',  'Carriage Return',           CharClass.CONTROL);
  static const SO  = const AsciiTable(14, 'SO',  'Shift Out',                 CharClass.CONTROL);
  static const SI  = const AsciiTable(15, 'SI',  'Shift In',                  CharClass.CONTROL);
  static const DLE = const AsciiTable(16, 'DLE', 'Data Link Escape',          CharClass.CONTROL);
  static const DC1 = const AsciiTable(17, 'DC1', 'Device Control 1 (XON)',    CharClass.CONTROL);
  static const DC2 = const AsciiTable(18, 'DC2', 'Device Control 2',          CharClass.CONTROL);
  static const DC3 = const AsciiTable(19, 'DC3', 'Device Control 3 (XOFF)',   CharClass.CONTROL);
  static const DC4 = const AsciiTable(20, 'DC4', 'Device Control 4',          CharClass.CONTROL);
  static const NAK = const AsciiTable(21, 'NAK', 'Negative Acknowledgment',   CharClass.CONTROL);
  static const SYN = const AsciiTable(22, 'SYN', 'Synchronous Idle',          CharClass.CONTROL);
  static const ETB = const AsciiTable(23, 'ETB', 'End of Transmission Block', CharClass.CONTROL);
  static const CAN = const AsciiTable(24, 'CAN', 'Cancel',                    CharClass.CONTROL);
  static const EM  = const AsciiTable(25, 'EM',  'End of Medium',             CharClass.CONTROL);
  static const SUB = const AsciiTable(26, 'SUB', 'Substitute',                CharClass.CONTROL);
  static const ESC = const AsciiTable(27, 'ESC', 'Escape',                    CharClass.CONTROL);
  static const FS  = const AsciiTable(28, 'FS',  'File Separator',            CharClass.CONTROL);
  static const GS  = const AsciiTable(29, 'GS',  'Group Separator',           CharClass.CONTROL);
  static const RS  = const AsciiTable(30, 'RS',  'Record Separator',          CharClass.CONTROL);
  static const US  = const AsciiTable(31, 'US',  'Unit Separator',            CharClass.CONTROL);

  // SP and SPACE are the same!
  static const SP                 = const AsciiTable(32, 'SP',                'Space',             CharClass.PUNCTUATION);
  static const SPACE              = const AsciiTable(32, 'SPACE',             'Space',             CharClass.PUNCTUATION);
  static const EXCLAMATION_MARK   = const AsciiTable(33, 'EXCLAMATION_MARK',  'Exclamation Mark',  CharClass.PUNCTUATION);
  static const SQUOTATION_MARKTX  = const AsciiTable(34, 'QUOTATION_MARK',    'Quotation Mark',    CharClass.PUNCTUATION);
  static const NUMBER_SIGN        = const AsciiTable(35, 'NUMBER_SIGN',       'Number sign',       CharClass.PUNCTUATION);
  static const DOLLAR_SIGN        = const AsciiTable(36, 'DOLLAR_SIGN',       'Dollar sign',       CharClass.PUNCTUATION);
  static const PERCENT_SIGN       = const AsciiTable(37, 'PERCENT_SIGN',      'Percent sign',      CharClass.PUNCTUATION);
  static const AMPERSAND          = const AsciiTable(38, 'AMPERSAND',         'Ampersand',         CharClass.PUNCTUATION);
  static const APOSTROPHE         = const AsciiTable(39, 'APOSTROPHE',        'Apostrophe',        CharClass.PUNCTUATION);
  static const LEFT_PARENTHESES   = const AsciiTable(40, 'LEFT_PARENTHESES',  'Left parentheses',  CharClass.PUNCTUATION);
  static const RIGTH_PARENTHESES  = const AsciiTable(41, 'RIGTH_PARENTHESES', 'Rigth parentheses', CharClass.PUNCTUATION);
  static const ASTERISK           = const AsciiTable(42, 'ASTERISK',          'Asterisk',          CharClass.PUNCTUATION);
  static const PLUS_SIGN          = const AsciiTable(43, 'PLUS_SIGN',         'Plus sign',         CharClass.PUNCTUATION);
  static const COMMA              = const AsciiTable(44, 'COMMA',             'Comma',             CharClass.PUNCTUATION);
  static const MINUS_SIGN         = const AsciiTable(45, 'MINUS_SIGN',        'Minus sign',        CharClass.PUNCTUATION);
  static const PERIOD             = const AsciiTable(46, 'PERIOD',            'Period',            CharClass.PUNCTUATION);
  static const SLASH              = const AsciiTable(47, 'SLASH',             'Slash',             CharClass.PUNCTUATION);
  static const SOLIDUS = SLASH;

  static const DIGIT_0 = const AsciiTable(48, 'DIGIT_0', 'Number 0', CharClass.DIGIT);
  static const DIGIT_1 = const AsciiTable(49, 'DIGIT_1', 'Number 1', CharClass.DIGIT);
  static const DIGIT_2 = const AsciiTable(50, 'DIGIT_2', 'Number 2', CharClass.DIGIT);
  static const DIGIT_3 = const AsciiTable(51, 'DIGIT_3', 'Number 3', CharClass.DIGIT);
  static const DIGIT_4 = const AsciiTable(52, 'DIGIT_4', 'Number 4', CharClass.DIGIT);
  static const DIGIT_5 = const AsciiTable(53, 'DIGIT_5', 'Number 5', CharClass.DIGIT);
  static const DIGIT_6 = const AsciiTable(54, 'DIGIT_6', 'Number 6', CharClass.DIGIT);
  static const DIGIT_7 = const AsciiTable(55, 'DIGIT_7', 'Number 7', CharClass.DIGIT);
  static const DIGIT_8 = const AsciiTable(56, 'DIGIT_8', 'Number 8', CharClass.DIGIT);
  static const DIGIT_9 = const AsciiTable(57, 'DIGIT_9', 'Number 9', CharClass.DIGIT);

  static const COLON             = const AsciiTable(58, 'COLON',             'Colon',             CharClass.PUNCTUATION);
  static const SEMICOLON         = const AsciiTable(59, 'SEMICOLON',         'Semicolon',         CharClass.PUNCTUATION);
  static const LESS_THAN_SIGN    = const AsciiTable(60, 'LESS_THAN_SIGN',    'Less-than sign',    CharClass.PUNCTUATION);
  static const EQUALS_SIGN       = const AsciiTable(61, 'EQUALS_SIGN',       'Equals sign',       CharClass.PUNCTUATION);
  static const GREATER_THAN_SIGN = const AsciiTable(62, 'GREATER_THAN_SIGN', 'Greater-than sign', CharClass.PUNCTUATION);
  static const QUESTION_MARK     = const AsciiTable(63, 'QUESTION_MARK',     'Question mark',     CharClass.PUNCTUATION);
  static const AT_SIGN           = const AsciiTable(64, 'AT_SIGN',           'At sign',           CharClass.PUNCTUATION);

  static const A = const AsciiTable(65, 'A', 'Letter A', CharClass.UPPERCASE);
  static const B = const AsciiTable(66, 'B', 'Letter B', CharClass.UPPERCASE);
  static const C = const AsciiTable(67, 'C', 'Letter C', CharClass.UPPERCASE);
  static const D = const AsciiTable(68, 'D', 'Letter D', CharClass.UPPERCASE);
  static const E = const AsciiTable(69, 'E', 'Letter E', CharClass.UPPERCASE);
  static const F = const AsciiTable(70, 'F', 'Letter F', CharClass.UPPERCASE);
  static const G = const AsciiTable(71, 'G', 'Letter G', CharClass.UPPERCASE);
  static const H = const AsciiTable(72, 'H', 'Letter H', CharClass.UPPERCASE);
  static const I = const AsciiTable(73, 'I', 'Letter I', CharClass.UPPERCASE);
  static const J = const AsciiTable(74, 'J', 'Letter J', CharClass.UPPERCASE);
  static const K = const AsciiTable(75, 'K', 'Letter K', CharClass.UPPERCASE);
  static const L = const AsciiTable(76, 'L', 'Letter L', CharClass.UPPERCASE);
  static const M = const AsciiTable(77, 'M', 'Letter M', CharClass.UPPERCASE);
  static const N = const AsciiTable(78, 'N', 'Letter N', CharClass.UPPERCASE);
  static const O = const AsciiTable(79, 'O', 'Letter O', CharClass.UPPERCASE);
  static const P = const AsciiTable(80, 'P', 'Letter P', CharClass.UPPERCASE);
  static const Q = const AsciiTable(81, 'Q', 'Letter Q', CharClass.UPPERCASE);
  static const R = const AsciiTable(82, 'R', 'Letter R', CharClass.UPPERCASE);
  static const S = const AsciiTable(83, 'S', 'Letter S', CharClass.UPPERCASE);
  static const T = const AsciiTable(84, 'T', 'Letter T', CharClass.UPPERCASE);
  static const U = const AsciiTable(85, 'U', 'Letter U', CharClass.UPPERCASE);
  static const V = const AsciiTable(86, 'V', 'Letter V', CharClass.UPPERCASE);
  static const W = const AsciiTable(87, 'W', 'Letter W', CharClass.UPPERCASE);
  static const X = const AsciiTable(88, 'X', 'Letter X', CharClass.UPPERCASE);
  static const Y = const AsciiTable(89, 'Y', 'Letter Y', CharClass.UPPERCASE);
  static const Z = const AsciiTable(90, 'Z', 'Letter Z', CharClass.UPPERCASE);

  static const LEFT_SQUARE_BRACKET  = const AsciiTable(91,  'LEFT_SQUARE_BRACKET',  'Left square bracket',  CharClass.PUNCTUATION);
  static const BACKSLASH            = const AsciiTable(92,  'BACKSLASH',            'Backslash',            CharClass.PUNCTUATION);
  static const REVERSE_SOLIDUS = BACKSLASH;
  static const RIGHT_SQUARE_BRACKET = const AsciiTable(93,  'RIGHT_SQUARE_BRACKET', 'Right square bracket', CharClass.PUNCTUATION);
  static const CARET                = const AsciiTable(94,  'CARET',                'Caret',                CharClass.PUNCTUATION);
  static const UNDERSCORE           = const AsciiTable(95,  'UNDERSCORE',           'Underscore',           CharClass.PUNCTUATION);
  static const GRAVE_ACCENT         = const AsciiTable(96,  'GRAVE_ACCENT',         'Grave accent',         CharClass.PUNCTUATION);

  static const a = const AsciiTable(97,  'a', 'Letter a',   CharClass.LOWERCASE);
  static const b = const AsciiTable(98,  'b', 'Letter b',   CharClass.LOWERCASE);
  static const c = const AsciiTable(99,  'c', 'Letter c',   CharClass.LOWERCASE);
  static const d = const AsciiTable(100, 'd', 'Letter d',   CharClass.LOWERCASE);
  static const e = const AsciiTable(101, 'e', 'Letter e',   CharClass.LOWERCASE);
  static const f = const AsciiTable(102, 'f', 'Letter f',   CharClass.LOWERCASE);
  static const g = const AsciiTable(103, 'g', 'Letter g',   CharClass.LOWERCASE);
  static const h = const AsciiTable(104, 'h', 'Letter h',   CharClass.LOWERCASE);
  static const i = const AsciiTable(105, 'i', 'Letter i',   CharClass.LOWERCASE);
  static const j = const AsciiTable(106, 'j', 'Letter j',   CharClass.LOWERCASE);
  static const k = const AsciiTable(107, 'k', 'Letter k',   CharClass.LOWERCASE);
  static const l = const AsciiTable(108, 'l', 'Letter l',   CharClass.LOWERCASE);
  static const m = const AsciiTable(109, 'm', 'Letter m',   CharClass.LOWERCASE);
  static const n = const AsciiTable(110, 'n', 'Letter n',   CharClass.LOWERCASE);
  static const o = const AsciiTable(111, 'o', 'Letter o',   CharClass.LOWERCASE);
  static const p = const AsciiTable(112, 'p', 'Letter p',   CharClass.LOWERCASE);
  static const q = const AsciiTable(113, 'q', 'Letter q',   CharClass.LOWERCASE);
  static const r = const AsciiTable(114, 'r', 'Letter r',   CharClass.LOWERCASE);
  static const s = const AsciiTable(115, 's', 'Letter s',   CharClass.LOWERCASE);
  static const t = const AsciiTable(116, 't', 'Letter t',   CharClass.LOWERCASE);
  static const u = const AsciiTable(117, 'u', 'Letter u',   CharClass.LOWERCASE);
  static const v = const AsciiTable(118, 'v', 'Letter v',   CharClass.LOWERCASE);
  static const w = const AsciiTable(119, 'w', 'Letter w',   CharClass.LOWERCASE);
  static const x = const AsciiTable(120, 'x', 'Letter x',   CharClass.LOWERCASE);
  static const y = const AsciiTable(121, 'y', 'Letter y',   CharClass.LOWERCASE);
  static const z = const AsciiTable(122, 'z', 'Letter z',   CharClass.LOWERCASE);

  static const LEFT_CURLY_BRACKET  = const AsciiTable(123,  'LEFT_CURLY_BRACKET',  'Left curly bracket',  CharClass.PUNCTUATION);
  static const VERTICAL_BAR        = const AsciiTable(124,  'VERTICAL_BAR',        'Vertical bar',        CharClass.PUNCTUATION);
  static const RIGHT_CURLY_BRACKET = const AsciiTable(125,  'RIGHT_CURLY_BRACKET', 'Right curly bracket', CharClass.PUNCTUATION);
  static const TILDE               = const AsciiTable(126,  'TILDE',               'Tilde',               CharClass.PUNCTUATION);
  static const DEL                 = const AsciiTable(127,  'DEL',                 'Delete',              CharClass.CONTROL);

  // Synonyms
  static const BACKSPACE = BS;
  static const TAB = HT;
  static const LINEFEED = LF;
  static const FORMFEED = FF;
  static const RETURN = CR;
  static const ESCAPE = ESC;
  static const DELETE = DEL;

  static const lookup = const [ NUL, SOH, STX, ETX, EOT, ENQ, ACK, BEL, BS, HT, LF, VT,
      FF, CR, SO, SI, DLE, DC1, DC2, DC3, DC4, NAK, SYN, ETB, CAN, EM, SUB, ESC, FS, GS,
      RS, US,

      //SP , (32,   'SP',  'Space',      AsciiType.Control); //SP or SPACE?
      SPACE, EXCLAMATION_MARK, SQUOTATION_MARKTX, NUMBER_SIGN, DOLLAR_SIGN, PERCENT_SIGN,
      AMPERSAND, APOSTROPHE, LEFT_PARENTHESES, RIGTH_PARENTHESES, ASTERISK, PLUS_SIGN,
      COMMA, MINUS_SIGN, PERIOD, SLASH,

      DIGIT_0, DIGIT_1, DIGIT_2, DIGIT_3, DIGIT_4, DIGIT_5, DIGIT_6, DIGIT_7,
      DIGIT_8, DIGIT_9,

      COLON, SEMICOLON, LESS_THAN_SIGN, EQUALS_SIGN, GREATER_THAN_SIGN, QUESTION_MARK,
      AT_SIGN,

      A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

      LEFT_SQUARE_BRACKET, BACKSLASH, RIGHT_SQUARE_BRACKET, CARET, UNDERSCORE,
      GRAVE_ACCENT,

      a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z,

      LEFT_CURLY_BRACKET, VERTICAL_BAR, RIGHT_CURLY_BRACKET, TILDE, DEL];

  toBinaryString()  => code.toRadixString(2);
  toDecimalString() => code.toRadixString(10);
  toHexString()     => code.toRadixString(16);
  toString()        => 'Ascii.$name=$code';

}

