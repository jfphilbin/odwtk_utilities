// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library ascii_char_class;

/**
 *  Defines a set of ASCII character classes.
 */
class CharClass {
  final int value;
  final String name;

  const CharClass(this.value, this.name);

  /// Control characters with codes from Nul=0 to xxx=31, plus Del=127. Whitespace characters,
  /// such as CR = carriage return and LF = linefeed are included in the Control class.
  static const CONTROL    = const CharClass(0, "Control");

  /// Uppercase characters 'A' - 'Z'.
  static const UPPERCASE  = const CharClass(1, "Uppercase");

  /// Lowercase characters 'a' - 'z'.
  static const LOWERCASE  = const CharClass(2, "Lowercase");

  /// Characters representing digits (numbers) '0' - '9'
  static const DIGIT     = const CharClass(3, "Number");

  /// Synonym for Digit.
  static const NUMBER      = DIGIT;

  /// Characters that represent whitespace in documents.
  static const WHITESPACE = const CharClass(4, "Whitespace");

  /// Punctuation characters
  static const PUNCTUATION = const CharClass(5, "Punctuation");

}