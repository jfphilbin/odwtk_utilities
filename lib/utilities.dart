// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library utilities;

export 'ascii.dart';
export 'benchmark.dart';
export 'enum.dart';
export 'indenter.dart';
export 'dcm_logging.dart';
export 'src/ascii/ascii_utils.dart';
export 'uuid.dart';
export 'version.dart';

/** Returns a [String] that corresponsed to 'name' of the [Symbol]. */
String symbolToString(Symbol symbol) {
  String s = symbol.toString();  // Symbol("foo")
  return s.substring(8, s.length - 2);
}
