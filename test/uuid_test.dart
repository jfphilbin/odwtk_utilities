// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:logging/logging.dart';
//import 'package:logging_handlers/server_logging_handlers.dart';
import 'package:utilities/dcm_logging.dart';
import 'package:utilities/uuid.dart';

String uuid1String = '00112233-4455-6677-8899-AABBCCDDEEFF';

void main() {

  startLogging('logs/uuid_test.log', hierarchical: true, level: Level.INFO);
  final log = new Logger("uuid_test");

  log.info(uuid1String);
  Uuid uuid1 = new Uuid.fromString(uuid1String);
  log.info(uuid1);
  Uuid uuid1a = new Uuid.fromString(uuid1.toString());
  log.info(uuid1 == uuid1a);
  log.info(uuid1.compareTo(uuid1a));

  int msb = uuid1.mostSigBits;
  log.info(msb);
  int lsb = uuid1.leastSigBits;
  log.warning(lsb);

  Uuid uuid2 = new Uuid.random();
  log.info('uuid2 = $uuid2');
  log.info('uuid2 as string = ${uuid2.toString()}');

}