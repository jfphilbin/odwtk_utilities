// DICOMweb Toolkit in Dart
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import '../ascii/ascii_class_table.dart';
import '../ascii/generate_source_code.dart';

/**
 * Generate An Ascii Lookup Table
 */

void main() {
  //TODO define List asciiTemplate = ["code", "name", "charClass", "doc", "synonyms"];

  var prefix = r"$_";
  var out = """
/// ASCII Map<String, int>
const Map<String, int> asciiMap = const
    {
""";

  for(List charset in usAsciiChars) {
    for(List def in charset) {
      int code              = def[0];
      String name           = def[1];
      String charClass      = def[2];
      String doc            = def[3];

      out += '    // $name:  $doc\n';
      out += '    \"$name\":  $code,\n';
      if (def.length == 5) {
        List<String> synonyms = def[4];
        if (synonyms.length > 0) {
          out += '      // $name - synonyms\n';
          for (String syn in synonyms) {
            out += '      \"$syn\": $code,\n';
          }
        }
      }
    }
  }
  out = out.substring(0, out.length - 2);
  out += '    };\n';
  writeGeneratedDartFile('ascii_map', 'part of ascii', out);
}
