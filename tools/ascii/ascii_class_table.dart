// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library ascii_class_table;

List asciiTemplate = ["code", "name", "charClass", "doc", "synonyms"];

List asciiChars = [
  //code  name          CharClass      Doc                    Synonyms
    // Begin Control Chars
    [  0, "NUL",        "Control",     "Null",                ["NULL"]],
    [  1, "SOH",        "Control",     "Start of Header"],
    [  2, "STX",        "Control",     "Start of Text"],
    [  3, "ETX",        "Control",     "End of Text"],
    [  4, "EOT",        "Control",     "End of Transmission"],
    [  5, "ENQ",        "Control",     "Enquiry"],
    [  6, "ACK",        "Control",     "Acknowledge"],
    [  7, "BEL",        "Control",     "Bell (beep)"],
    // Whitespace Control Chars are here
    [  8, "BS",         "Whitespace", "Backspace",            ["BACKSPACE"]],
    [  9, "HT",         "Whitespace", "Horizontal Tab",       ["HTAB", "TAB"]],
    [ 10, "LF",         "Whitespace", "Line Feed",            ["LINEFEED", "NEWLINE"]],
    [ 11, "VT",         "Whitespace", "Vertical Tab",         ["VTAB"]],
    [ 12, "FF",         "Whitespace", "Form Feed",            ["FORMFEED"]],
    [ 13, "CR",         "Whitespace", "Carriage Return",      ["RETURN"]],
    // End of Whitespace
    [ 14, "SO",         "Control",     "Shift Out"],
    [ 15, "SI",         "Control",     "Shift In"],
    [ 16, "DLE",        "Control",     "Data Link Escape"],
    [ 17, "DC1",        "Control",     "Device Control 1 (XON)"],
    [ 18, "DC2",        "Control",     "Device Control 2"],
    [ 19, "DC3",        "Control",     "Device Control 3 (XOFF)"],
    [ 20, "DC4",        "Control",     "Device Control 4"],
    [ 21, "NAK",        "Control",     "Negative Acknowledge"],
    [ 22, "SYN",        "Control",     "Synchronous Idle"],
    [ 23, "ETB",        "Control",     "End of Transmission Block"],
    [ 24, "CAN",        "Control",     "Cancel"],
    [ 25, "EM",         "Control",     "End of Medium"],
    [ 26, "SUB",        "Control",     "Substitute"],
    [ 27, "ESC",        "Control",     "Escape"],
    [ 28, "FS",         "Control",     "File Separator"],
    [ 29, "GS",         "Control",     "Group Separator"],
    [ 30, "RS",         "Control",     "Record Separator"],
    [ 31, "US",         "Control",     "Unit Separator"],
    // End initial control chars

    // Space character
    [ 32, "SP",         "Whitespace", "Space",                ["SPACE"]],

    // Punctuation
    [ 33, "EXCLAM",     "Punctuation", "Exclamation Mark",    ["EXCLAMATION_MARK", "EXCLAMATION"]],
    [ 34, "QUOTE",      "Punctuation", "Quotation Mark",      ["QUOTATION_MARK", "QUOTATION", "DOUBLE_QUOTE"]],
    [ 35, "NUMBER",     "Punctuation", "Number Sign",         ["NUMBER_SIGN"]],
    [ 36, "DOLLAR",     "Punctuation", "Dollar Sign",         ["DOLLAR_SIGN"]],
    [ 37, "PERCENT",    "Punctuation", "Percent Sign",        ["PERCENT_SIGN"]],
    [ 38, "AMPER",      "Punctuation", "Ampersand",           ["AMPERSAND"]],
    [ 39, "APOSTR",     "Punctuation", "Apostrophe",          ["APOSTROPHE", "SINGLE_QUOTE"]],
    [ 40, "LParen",     "Punctuation", "Left Parentheses",    ["LEFT_PARENTHESES"]],
    [ 41, "RPAREN",     "Punctuation", "Rigth Parentheses",   ["RIGHT_PARENTHESES"]],
    [ 42, "STAR",       "Punctuation", "Asterisk",            ["ASTERISK"]],
    [ 43, "PLUS",       "Punctuation", "Plus Sign",           ["PLUS_SIGN"]],
    [ 44, "COMMA",      "Punctuation", "Comma"],
    [ 45, "MINUS",      "Punctuation", "Hyphen, Minus Sign",  ["MINUS_SIGN", "HYPHEN"]],
    [ 46, "PERIOD",     "Punctuation", "Period, Full Stop",   ["FULL_STOP"]],
    [ 47, "SLASH",      "Punctuation", "Solidus, Slash",      ["SOLIDUS"]],

    // Digits
    [ 48, "DIGIT_0",    "Digit",       "Digit Zero",          ["0"]],
    [ 49, "DIGIT_1",    "Digit",       "Digit One",           ["1"]],
    [ 50, "DIGIT_2",    "Digit",       "Digit Two",           ["2"]],
    [ 51, "DIGIT_3",    "Digit",       "Digit Three",         ["3"]],
    [ 52, "DIGIT_4",    "Digit",       "Digit Four",          ["4"]],
    [ 53, "DIGIT_5",    "Digit",       "Digit Five",          ["5"]],
    [ 54, "DIGIT_6",    "Digit",       "Digit Six",           ["6"]],
    [ 55, "DIGIT_7",    "Digit",       "Digit Seven",         ["7"]],
    [ 56, "DIGIT_8",    "Digit",       "Digit Eight",         ["8"]],
    [ 57, "DIGIT_9",    "Digit",       "Digit Nine",          ["9"]],

    // Punctuation
    [ 58, "COLON",      "Punctuation", "Colon"],
    [ 59, "SEMI",       "Punctuation", "Semicolon",           ["SEMICOLON"]],
    [ 60, "LESS",       "Punctuation", "Less-Than Sign, Left Angle Bracket", ["LESS_THAN_SIGN", "LESS_THAN", "LEFT_ANGLE"]],
    [ 61, "EQUAL",      "Punctuation", "Equals Sign",         ["EQUALS_SIGN"]],
    [ 62, "GREATER",    "Punctuation", "Greater-Than Sign, Rigth Angle Bracket", ["GREATER_THAN_SIGN", "GREATER_THAN", "RIGHT_ANGLE"]],
    [ 63, "QUESTION",   "Punctuation", "Question Mark",       ["QUESTION_MARK"]],
    [ 64, "AT_SIGN",    "Punctuation", "Commeration At Sign", ["COMMERCIAL_AT_SIGN", "AT"]],

    // Uppercase Chars are here
    [ 65, "A",          "Uppercase",   "Capital Letter A"],
    [ 66, "B",          "Uppercase",   "Capital Letter B"],
    [ 67, "C",          "Uppercase",   "Capital Letter C"],
    [ 68, "D",          "Uppercase",   "Capital Letter D"],
    [ 69, "E",          "Uppercase",   "Capital Letter E"],
    [ 70, "F",          "Uppercase",   "Capital Letter F"],
    [ 71, "G",          "Uppercase",   "Capital Letter G"],
    [ 72, "H",          "Uppercase",   "Capital Letter H"],
    [ 73, "I",          "Uppercase",   "Capital Letter I"],
    [ 74, "J",          "Uppercase",   "Capital Letter J"],
    [ 75, "K",          "Uppercase",   "Capital Letter K"],
    [ 76, "L",          "Uppercase",   "Capital Letter L"],
    [ 77, "M",          "Uppercase",   "Capital Letter M"],
    [ 78, "N",          "Uppercase",   "Capital Letter N"],
    [ 79, "O",          "Uppercase",   "Capital Letter O"],
    [ 80, "P",          "Uppercase",   "Capital Letter P"],
    [ 81, "Q",          "Uppercase",   "Capital Letter Q"],
    [ 82, "R",          "Uppercase",   "Capital Letter R"],
    [ 83, "S",          "Uppercase",   "Capital Letter S"],
    [ 84, "T",          "Uppercase",   "Capital Letter T"],
    [ 85, "U",          "Uppercase",   "Capital Letter U"],
    [ 86, "V",          "Uppercase",   "Capital Letter V"],
    [ 87, "W",          "Uppercase",   "Capital Letter W"],
    [ 88, "X",          "Uppercase",   "Capital Letter X"],
    [ 89, "Y",          "Uppercase",   "Capital Letter Y"],
    [ 90, "Z",          "Uppercase",   "Capital Letter Z"],

    // Punctuation
    [ 91, "LSQUARE",    "Punctuation", "Left Square Bracket", ["LEFT_SQUARE_BRACKET"]],
    [ 92, "BACKSLASH",  "Punctuation", "Reverse Solidus (Backslash)", ["REVERSE_SOLIDUS"]],
    [ 93, "RSQUARE",    "Punctuation", "Right Square Bracket", ["RIGHT_SQUARE_BRACKET"]],
    [ 94, "CIRCOMFLEX", "Punctuation", "Circumflex Accent",   ["CIRCOMFLEX_ACCENT"]],
    [ 95, "LOW_LINE",   "Punctuation", "Low Line, Underline, Underscore", ["_", "UNDERLINE", "UNDERSCORE"]],
    [ 96, "GRAVE",      "Punctuation", "Grave Accent",        ["GRAVE_ACCENT"]],

    // Lowercase Chars are here
    [ 97, "a",          "Lowercase",   "Small Letter a"],
    [ 98, "b",          "Lowercase",   "Small Letter b"],
    [ 99, "c",          "Lowercase",   "Small Letter c"],
    [100, "d",          "Lowercase",   "Small Letter d"],
    [101, "e",          "Lowercase",   "Small Letter e"],
    [102, "f",          "Lowercase",   "Small Letter f"],
    [103, "g",          "Lowercase",   "Small Letter g"],
    [104, "h",          "Lowercase",   "Small Letter h"],
    [105, "i",          "Lowercase",   "Small Letter i"],
    [106, "j",          "Lowercase",   "Small Letter j"],
    [107, "k",          "Lowercase",   "Small Letter k"],
    [108, "l",          "Lowercase",   "Small Letter l"],
    [109, "m",          "Lowercase",   "Small Letter m"],
    [110, "n",          "Lowercase",   "Small Letter n"],
    [111, "o",          "Lowercase",   "Small Letter o"],
    [112, "p",          "Lowercase",   "Small Letter p"],
    [113, "q",          "Lowercase",   "Small Letter q"],
    [114, "r",          "Lowercase",   "Small Letter r"],
    [115, "s",          "Lowercase",   "Small Letter s"],
    [116, "t",          "Lowercase",   "Small Letter t"],
    [117, "u",          "Lowercase",   "Small Letter u"],
    [118, "v",          "Lowercase",   "Small Letter v"],
    [119, "w",          "Lowercase",   "Small Letter w"],
    [120, "x",          "Lowercase",   "Small Letter x"],
    [121, "y",          "Lowercase",   "Small Letter y"],
    [122, "z",          "Lowercase",   "Small Letter z"],

    // Punctuation
    [123, "LBRACE",     "Punctuation", "Left Curly Bracket, Left Brace", ["LEFT_CURLY_BRACKET", "LCURLY"]],
    [124, "VBAR",       "Punctuation", "Vertical Line, Vertical Bar", ["VERTICAL_LINE", "VLINE", "VERTICAL_BAR"]],
    [125, "RBRACE",     "Punctuation", "Right Curly Bracket, Right Brace", ["RIGHT_CURLY_BRACKET", "RCURLY"]],
    [126, "TILDE",      "Punctuation", "Tilde"],

    // Last Control char
    [127, "DEL",        "Control",     "Delete",              ["DELETE"]]];

List<int> control    = asciiChars.sublist(0, 32)
                             ..add(asciiChars[127]);

List<int> whitespace = asciiChars.sublist(8, 14)
                           ..add(asciiChars[32]);

List<int> digits      = asciiChars.sublist(48, 58);
List<int> uppercase   = asciiChars.sublist(65, 91);
List<int> lowercase   = asciiChars.sublist(97, 123);

List<int> punctuation = new List()
                            ..addAll(asciiChars.sublist(33, 48))
                            ..addAll(asciiChars.sublist(58, 65))
                            ..addAll(asciiChars.sublist(91,97))
                            ..addAll(asciiChars.sublist(123, 127));

/*

List asciiControlChars = [
  //code  name          CharClass      Doc                    Synonyms
    [  0, "NUL",        "Control",     "Null",                ["NULL"]],
    [  1, "SOH",        "Control",     "Start of Header"],
    [  2, "STX",        "Control",     "Start of Text"],
    [  3, "ETX",        "Control",     "End of Text"],
    [  4, "EOT",        "Control",     "End of Transmission"],
    [  5, "ENQ",        "Control",     "Enquiry"],
    [  6, "ACK",        "Control",     "Acknowledge"],
    [  7, "BEL",        "Control",     "Bell (beep)"],
    // Whitespace Control Chars are here
    [ 14, "SO",         "Control",     "Shift Out"],
    [ 15, "SI",         "Control",     "Shift In"],
    [ 16, "DLE",        "Control",     "Data Link Escape"],
    [ 17, "DC1",        "Control",     "Device Control 1 (XON)"],
    [ 18, "DC2",        "Control",     "Device Control 2"],
    [ 19, "DC3",        "Control",     "Device Control 3 (XOFF)"],
    [ 20, "DC4",        "Control",     "Device Control 4"],
    [ 21, "NAK",        "Control",     "Negative Acknowledge"],
    [ 22, "SYN",        "Control",     "Synchronous Idle"],
    [ 23, "ETB",        "Control",     "End of Transmission Block"],
    [ 24, "CAN",        "Control",     "Cancel"],
    [ 25, "EM",         "Control",     "End of Medium"],
    [ 26, "SUB",        "Control",     "Substitute"],
    [ 27, "ESC",        "Control",     "Escape"],
    [ 28, "FS",         "Control",     "File Separator"],
    [ 29, "GS",         "Control",     "Group Separator"],
    [ 30, "RS",         "Control",     "Record Separator"],
    [ 31, "US",         "Control",     "Unit Separator"],
    [127, "DEL",        "Control",     "Delete",              ["DELETE"]]];

const List asciiWhitespaceChars = const [
    const [  8, "BS",         "Whitespace", "Backspace",           const ["BACKSPACE"]],
    const [  9, "HT",         "Whitespace", "Horizontal Tab",      const ["HTAB", "TAB"]],
    const [ 10, "LF",         "Whitespace", "Line Feed",           const ["LINEFEED", "NEWLINE"]],
    const [ 11, "VT",         "Whitespace", "Vertical Tab",        const ["VTAB"]],
    const [ 12, "FF",         "Whitespace", "Form Feed",           const ["FORMFEED"]],
    const [ 13, "CR",         "Whitespace", "Carriage Return",     const ["RETURN"]],
    const [ 32, "SP",         "Whitespace", "Space",               const ["SPACE"]]];


List asciiDigitChars = [
[ 48, "DIGIT_0",    "Digit",       "Digit Zero",          ["0"]],
[ 49, "DIGIT_1",    "Digit",       "Digit One",           ["1"]],
[ 50, "DIGIT_2",    "Digit",       "Digit Two",           ["2"]],
[ 51, "DIGIT_3",    "Digit",       "Digit Three",         ["3"]],
[ 52, "DIGIT_4",    "Digit",       "Digit Four",          ["4"]],
[ 53, "DIGIT_5",    "Digit",       "Digit Five",          ["5"]],
[ 54, "DIGIT_6",    "Digit",       "Digit Six",           ["6"]],
[ 55, "DIGIT_7",    "Digit",       "Digit Seven",         ["7"]],
[ 56, "DIGIT_8",    "Digit",       "Digit Eight",         ["8"]],
[ 57, "DIGIT_9",    "Digit",       "Digit Nine",          ["9"]]
];

List asciiUppercaseChars = [
[ 65, "A",          "Uppercase",   "Capital Letter A"],
[ 66, "B",          "Uppercase",   "Capital Letter B"],
[ 67, "C",          "Uppercase",   "Capital Letter C"],
[ 68, "D",          "Uppercase",   "Capital Letter D"],
[ 69, "E",          "Uppercase",   "Capital Letter E"],
[ 70, "F",          "Uppercase",   "Capital Letter F"],
[ 71, "G",          "Uppercase",   "Capital Letter G"],
[ 72, "H",          "Uppercase",   "Capital Letter H"],
[ 73, "I",          "Uppercase",   "Capital Letter I"],
[ 74, "J",          "Uppercase",   "Capital Letter J"],
[ 75, "K",          "Uppercase",   "Capital Letter K"],
[ 76, "L",          "Uppercase",   "Capital Letter L"],
[ 77, "M",          "Uppercase",   "Capital Letter M"],
[ 78, "N",          "Uppercase",   "Capital Letter N"],
[ 79, "O",          "Uppercase",   "Capital Letter O"],
[ 80, "P",          "Uppercase",   "Capital Letter P"],
[ 81, "Q",          "Uppercase",   "Capital Letter Q"],
[ 82, "R",          "Uppercase",   "Capital Letter R"],
[ 83, "S",          "Uppercase",   "Capital Letter S"],
[ 84, "T",          "Uppercase",   "Capital Letter T"],
[ 85, "U",          "Uppercase",   "Capital Letter U"],
[ 86, "V",          "Uppercase",   "Capital Letter V"],
[ 87, "W",          "Uppercase",   "Capital Letter W"],
[ 88, "X",          "Uppercase",   "Capital Letter X"],
[ 89, "Y",          "Uppercase",   "Capital Letter Y"],
[ 90, "Z",          "Uppercase",   "Capital Letter Z"],
];

List asciiLowercaseChars = [
[ 97, "a",          "Lowercase",   "Small Letter a"],
[ 98, "b",          "Lowercase",   "Small Letter b"],
[ 99, "c",          "Lowercase",   "Small Letter c"],
[100, "d",          "Lowercase",   "Small Letter d"],
[101, "e",          "Lowercase",   "Small Letter e"],
[102, "f",          "Lowercase",   "Small Letter f"],
[103, "g",          "Lowercase",   "Small Letter g"],
[104, "h",          "Lowercase",   "Small Letter h"],
[105, "i",          "Lowercase",   "Small Letter i"],
[106, "j",          "Lowercase",   "Small Letter j"],
[107, "k",          "Lowercase",   "Small Letter k"],
[108, "l",          "Lowercase",   "Small Letter l"],
[109, "m",          "Lowercase",   "Small Letter m"],
[110, "n",          "Lowercase",   "Small Letter n"],
[111, "o",          "Lowercase",   "Small Letter o"],
[112, "p",          "Lowercase",   "Small Letter p"],
[113, "q",          "Lowercase",   "Small Letter q"],
[114, "r",          "Lowercase",   "Small Letter r"],
[115, "s",          "Lowercase",   "Small Letter s"],
[116, "t",          "Lowercase",   "Small Letter t"],
[117, "u",          "Lowercase",   "Small Letter u"],
[118, "v",          "Lowercase",   "Small Letter v"],
[119, "w",          "Lowercase",   "Small Letter w"],
[120, "x",          "Lowercase",   "Small Letter x"],
[121, "y",          "Lowercase",   "Small Letter y"],
[122, "z",          "Lowercase",   "Small Letter z"],
];

List asciiPunctuationChars = [
// Control Chars are here
[ 33, "EXCLAM",     "Punctuation", "Exclamation Mark",    ["EXCLAMATION_MARK", "EXCLAMATION"]],
[ 34, "QUOTE",      "Punctuation", "Quotation Mark",      ["QUOTATION_MARK", "QUOTATION", "DOUBLE_QUOTE"]],
[ 35, "NUMBER",     "Punctuation", "Number Sign",         ["NUMBER_SIGN"]],
[ 36, "DOLLAR",     "Punctuation", "Dollar Sign",         ["DOLLAR_SIGN"]],
[ 37, "PERCENT",    "Punctuation", "Percent Sign",        ["PERCENT_SIGN"]],
[ 38, "AMPER",      "Punctuation", "Ampersand",           ["AMPERSAND"]],
[ 39, "APOSTR",     "Punctuation", "Apostrophe",          ["APOSTROPHE", "SINGLE_QUOTE"]],
[ 40, "LParen",     "Punctuation", "Left Parentheses",    ["LEFT_PARENTHESES"]],
[ 41, "RPAREN",     "Punctuation", "Rigth Parentheses",   ["RIGHT_PARENTHESES"]],
[ 42, "STAR",       "Punctuation", "Asterisk",            ["ASTERISK"]],
[ 43, "PLUS",       "Punctuation", "Plus Sign",           ["PLUS_SIGN"]],
[ 44, "COMMA",      "Punctuation", "Comma"],
[ 45, "MINUS",      "Punctuation", "Hyphen, Minus Sign",  ["MINUS_SIGN", "HYPHEN"]],
[ 46, "PERIOD",     "Punctuation", "Period, Full Stop",   ["FULL_STOP"]],
[ 47, "SLASH",      "Punctuation", "Solidus, Slash",      ["SOLIDUS"]],
// Digit Chars are here
[ 58, "COLON",      "Punctuation", "Colon"],
[ 59, "SEMI",       "Punctuation", "Semicolon",           ["SEMICOLON"]],
[ 60, "LESS",       "Punctuation", "Less-Than Sign, Left Angle Bracket", ["LESS_THAN_SIGN", "LESS_THAN", "LEFT_ANGLE"]],
[ 61, "EQUAL",      "Punctuation", "Equals Sign",         ["EQUALS_SIGN"]],
[ 62, "GREATER",    "Punctuation", "Greater-Than Sign, Rigth Angle Bracket", ["GREATER_THAN_SIGN", "GREATER_THAN", "RIGHT_ANGLE"]],
[ 63, "QUESTION",   "Punctuation", "Question Mark",       ["QUESTION_MARK"]],
[ 64, "AT_SIGN",    "Punctuation", "Commeration At Sign", ["COMMERCIAL_AT_SIGN", "AT"]],
// Uppercase Chars are here
[ 91, "LSQUARE",    "Punctuation", "Left Square Bracket", ["LEFT_SQUARE_BRACKET"]],
[ 92, "BACKSLASH",  "Punctuation", "Reverse Solidus (Backslash)", ["REVERSE_SOLIDUS"]],
[ 93, "RSQUARE",    "Punctuation", "Right Square Bracket", ["RIGHT_SQUARE_BRACKET"]],
[ 94, "CIRCOMFLEX", "Punctuation", "Circumflex Accent",   ["CIRCOMFLEX_ACCENT"]],
[ 95, "LOW_LINE",   "Punctuation", "Low Line, Underline, Underscore", ["_", "UNDERLINE", "UNDERSCORE"]],
[ 96, "GRAVE",      "Punctuation", "Grave Accent",        ["GRAVE_ACCENT"]],
// Lowercase Chars are here
[123, "LBRACE",     "Punctuation", "Left Curly Bracket, Left Brace", ["LEFT_CURLY_BRACKET", "LCURLY"]],
[124, "VBAR",       "Punctuation", "Vertical Line, Vertical Bar", ["VERTICAL_LINE", "VLINE", "VERTICAL_BAR"]],
[125, "RBRACE",     "Punctuation", "Right Curly Bracket, Right Brace", ["RIGHT_CURLY_BRACKET", "RCURLY"]],
[126, "TILDE",      "Punctuation", "Tilde"],
];
*/
// US ASCII Constants
List usAsciiChars = [control, whitespace, digits, uppercase, lowercase, punctuation];
