// DICOMweb Toolkit in Dart
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'ascii_class_table.dart';
import 'generate_source_code.dart';

void main() {
  //TODO define List asciiTemplate = ["code", "name", "charClass", "doc", "synonyms"];

  var prefix = r"$_";
  var out = asciiConstantsPrefix;

  for(List charset in usAsciiChars) {
    out += '/// Control Characters';
    for(List def in charset) {
      int code              = def[0];
      String name           = def[1];
      String charClass      = def[2];
      String doc            = def[3];

      out += '/// $name:  $doc\n';
      out += 'const $prefix$name = $code;\n';
      if (def.length == 5) {
        List<String> synonyms = def[4];
        if (synonyms.length > 0) {
          out += '    // $name - synonyms\n';
          for (String syn in synonyms) {
            out += '    const $prefix$syn = $prefix$name;\n';
          }
        }
      }
      out += asciiConstantsSuffix;
    }
  }
  writeGeneratedDartFile('ascii_constants', 'part of ascii', out);
}

String asciiConstantsPrefix = """
/**
 * --The US ASCII Character Set--
 *
 * The codes and strings used in this file are defined in
 * ANSI X3.4-1986 and
 * ISO 646 International Reference Version
 *
 * "Codes 0 through 31 and 127 are unprintable control characters.
 *  Code 32 (decimal) is a non-printing spacing character.  Codes
 *  33 through 126 (decimal) are printable graphic characters."
 * See <http://www.columbia.edu/kermit/ascii.html>.
 *
 * The names and descriptions in the table below are from the
 * report; however, in the report the descriptions are all capital
 * or uppercase letters, and in the usAsciiMap below they are in both
 * upper and lowercase for readability.
 *
 * --Ascii Constants Library--
 *
 *   Note: This library is designed to be imported with a name, e.g. 
 *
 *         "import 'package:utilities/ascii_constants.dart' as Ascii;"
 *       
 *         And then accessed using that name, e.g. Ascii.SPACE or Ascii.Z.
 *
 */
""";

String asciiConstantsSuffix = """

// End Ascii Constants
""";